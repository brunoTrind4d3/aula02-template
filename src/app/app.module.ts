import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { MenuComponent } from "./menu/menu.component";
import { ProdutoComponent } from "./produto/produto.component";
import { ClienteComponent } from "./cliente/cliente.component";
import { HomeComponent } from "./home/home.component";
import { RouterModule } from "@angular/router";
import { ROUTES } from "./app.route";

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ProdutoComponent,
    ClienteComponent,
    HomeComponent
  ],
  imports: [BrowserModule, AppRoutingModule, RouterModule.forRoot(ROUTES)],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
